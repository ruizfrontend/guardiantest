# Guardian interactive developer task

As I would do in a regular project I started preprocessing our data in PHP. I've used some functions I already have to query google maps API and export to JSON. I have some problem with encodings and lost a few time trying to get all of them programatically (there are arround 6 countries whithout the correct coordinates and 3 incorrectly over Australia!?#), so probably I should have look for a table with the data.

Anyway, I had enough time to prepare a decent front end. going with the basics, but trying to polish what I had and working without problems: jquery, bootstrap and google maps using infowindows, and basic markers as you required in the instructions, even that a cloropleth would be my choice for this kind of map, probaby with d3 or directly svg. Maybe another time.


David Ruiz López
August 24, 2014

---




Hello,

Your task is to create an interactive containing a map and accompanying table using the provided GDP.csv data.

The data MUST be loaded via XHR.

The map should contain markers in the correct location for every country listed with a GDP value.
Clicking a marker will show the country name and GDP value.

The table will contain the country names and GDP values.

    +----------------------------+
    |  Country name |     GDP    |
    +---------------+------------+
    | United States | 16,800,000 |
    |     ...       |     ...    |
    |     ...       |     ...    |
    +----------------------------+

Use whatever tools, additional data, libraries or frameworks you wish.

Please comment the code with your workings out.

Good luck.