<?php 

/** 
* dataProcessor.php
* Process 
* --------------------------------
**/

  // inicializando
setlocale(LC_ALL,"es_ES");

header('Content-Type: text/html; charset=utf-8');

error_reporting(E_ALL ^ E_NOTICE);
ini_set("display_startup_errors ",1); 
ini_set("error_reporting",E_ALL); 
ini_set('display_errors', 1);

ob_start('ob_gzhandler');

// deshabilita limite de tiempo
set_time_limit(0);

define('SITE_ROOT', dirname(__FILE__));




  /* PArse the CSV */
$dataCSV = parse_csv(file_get_contents(SITE_ROOT . '/GDP.csv'));

$dataJSON = processGDPS($dataCSV);

writeJson(SITE_ROOT . '/GDP.json', $dataJSON);



/*
processGDPS($data) 
-------------------------------------------------------------------------------------------------------------------------- 
Process the data from the CSV, gets the locations for the markers and returns an array with the data
$data                 -> Guardian task GDP csv data
*/
function processGDPS($data) {

    // the final data
  $outp = array();

    // loop trought the data
  foreach ($data as $row) {
      // ignore empty lines and comments
    if(!empty($row[0]) && !empty($row[1]) && !empty($row[3]) && !empty($row[4])) {
      
        // just checking
      //echo($row[0].' - '.$row[1].' - '.$row[3].' - '.$row[4].'<br/>');

      $position = queryPosition('country: '.$row[3]);

      $rowOK = array(
        'country' => $row[3],
        'location' => $position,
        'gdp' => $row[4]
      );

      array_push($outp, $rowOK);

    }
  }

  return $outp;
}



/* Query Google maps geolocation
      ---------------*/
function queryPosition($search) {

  $search = str_replace(" ", "+", $search);

  $url = "http://maps.googleapis.com/maps/api/geocode/json?address=".$search."&sensor=false";

  $geocode = file_get_contents($url);

  $output = json_decode($geocode);

  if ($output->status == 'OK' && count($output->results)) {

    $return = array(
      'full' => $output->results[0]->formatted_address,
      'lat' => $output->results[0]->geometry->location->lat,
      'lng' => $output->results[0]->geometry->location->lng
    );

    return $return;

  }

  return false;

}


/*
writeJson() 
-------------------------------------------------------------------------------------------------------------------------- 
$file                 -> Path of the file to write
$data                 -> Array to export to the file
*/
function writeJson($file, $data) {
  $file = fopen($file, 'w');
  fwrite($file, json_encode($data));
  fclose($file);

  return true;
}



/*
parse_csv()
------------------------------------------------------------------------------------------------------------------------------ 

Método que parsea un fichero CSV en un array

Parametros de entrada:

$csv_string           -> String con la información del CSV
$delimiter            -> Delimitador usado en el CSV
$skip_empty_lines     -> Flag que indica si se saltan líneas vacias
$trim_fields          -> Flag que indica si se limpian los espacios en blanco al principio/final de cada uno de los campos
*/
function parse_csv ($csv_string, $delimiter = ",", $skip_empty_lines = true, $trim_fields = false) {

  $enc = preg_replace('/(?<!")""/', '!!Q!!', $csv_string);
  $enc = preg_replace_callback(
    '/"(.*?)"/s',
    function ($field) { return urlencode(utf8_encode($field[1])); },
    $enc
  );
  
  $lines = preg_split($skip_empty_lines ? ($trim_fields ? '/( *\R)+/s' : '/\R+/s') : '/\R/s', $enc);
  
  return array_map(
    function ($line) use ($delimiter, $trim_fields) {
      $fields = $trim_fields ? array_map('trim', explode($delimiter, $line)) : explode($delimiter, $line);
      return array_map(
        function ($field) { return str_replace('!!Q!!', '"', utf8_decode(urldecode($field))); },
        $fields
      );
    },
    $lines
  );
};